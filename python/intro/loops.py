def main():

	# There are two types of loops available in Python:
	# - The for loop, which does some thing for every element in a list
	# - The while loop, which does something until a boolean expression
	#   returns false.

	l = ['a', 'b', 'c']

	print("Simple loop over the list ['a', 'b', 'c']:")
	for i in l:
		print(i)
	
	# To have a plain old integer for loop, use range. Do notice that
	# range starts at 0, and goes to the integer supplied - 1:

	print("Simple loop over range(5):")
	for i in range(5):
		print(i)
	
	# The continue statement can be used to skip to the next iteration

	print("This loop will only print odd numbers in range(5):")
	for i in range(5):
		if i % 2 == 0:
			continue
		print(i)
	
	print("The break statement will terminate the loop prematurely, this" +
			" whill stop after 2 in range(5):")
	for i in range(5):
		if i > 2:
			break
		print(i)

	# The while loop will execute until the boolean expression is false
	print("x = 1, doubling until x >= 100")
	x = 1
	while (x < 100):
		x += x
	
	print("x = {}".format(x))

if __name__ == "__main__":
	main()