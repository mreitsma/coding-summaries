def main():
	# There are various forms of sequences in Python. Two of the most
	# used ones are lists and tuples.

	# Lists are ordered sequences of values:

	l1 = [1, 3, 3, 7]

	print("The list: {}".format(l1))
	print("in a for loop, 'for i in l1: print (i)':")
	for i in l1:
		print(i)

	# Lists are using indices from 0 for the elements. The len function
	# returns the number of elements in the list. Combine both for the
	# following loop:
	for i in range(len(l1)):
		print("l1[{}] = {}".format(i, l1[i]))

	# The if and in keywords can be used to check if a certain value is in a
	# sequence:
	if 3 in l1:
		print("The value 3 is in the list l1: {}".format(l1))
	if 123 in l1:
		print("The value 123 is not in the list l1, this will not print")

	# Negative indices start counting from the end, and using -1 as last element:
	print("l1[-1] = {}".format(l1[-1]))

	# Lists can be added together:

	l2 = [8, 1, 4, 4, 7]

	print("l1 = {}, l2 = {}".format(l1, l2))
	l3 = l1 + l2

	print("l3 = l1 + l2 = {}".format(l3))

	# Lists can be sliced using the indices.

	# Getting a 'sub' list from the second element:
	print("l3[2:] = {}".format(l3[2:]))

	# Getting a sub list from the third till the fifth element exclusive (= 4th element):
	print("l3[2:5] = {}".format(l3[2:5]))

	# Getting a sublist with only the last two elements:
	print("l3[:-2] = {}".format(l3[-2:]))

	# Getting a sublist with the second till the second to last element:
	print("l3[2:-2] = {}".format(l3[2:-2]))

	# Adding things to the end of the list:
	l3.append(5)
	print("l3 after l3.append(5): {}".format(l3))

	# Remove the first occurence of a value:
	l3.remove(3)
	print("l3 after l3.remove(3), which removes the first 3: {}".format(l3))

	# Popping values from the end of the list:
	i = l3.pop()
	print("i = l3.pop() = {}".format(i))
	print("l3 = {}".format(l3))

	# A tuple is an immutable list, and is a comma seperated list of values:
	t1 = 1, 3, 3, 7

	# Round brackets can be used when working with a tuple:
	t2 = (8, 1, 4, 4, 7)

	print("t1 = {}, t2 = {}".format(t1, t2))

	# Same intersection and addition rules apply:
	t3 = t1 + t2
	print("t3 = t1 + t2 = {}".format(t3))
	print("t3[2:-4] = {}".format(t3[2:-4]))

	# Lists can be converted to tuples, and tuples to lists:
	t = ('t', 'u', 'p', 'l', 'e')
	print("Tuple t = {}, in list form: {}".format(t, list(t)))
	l = ['l', 'i', 's' ,'t']
	print("List l = {}, in tuple form: {}".format(l, tuple(l)))

	# Do note, lists and tuples are unordered! Lists can be ordered:
	unordered_list = [3, 7, 1, 2, 10, 5, 3]
	print("Unordered list: {}".format(unordered_list))

	# Sorted does return a new list, and leaves the original list untouched
	sorted_list = sorted(unordered_list)
	print("Sorted list: {}".format(sorted_list))
	print("Original list: {}".format(unordered_list))




if __name__ == "__main__":
	main()