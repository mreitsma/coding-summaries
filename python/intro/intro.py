def main():
	# Few of the basic functions:
	# print, includes a newline
	print("This line is printed to the console")
	print("This is the second line that is printed to the console")

	# Simple string formatting can be done using the format method on
	# any string. The format method will replace curly braces with the
	# values that are passed to the format method.

	fmt = "format"
	print("This is a {}, using the {} method".format("String", fmt))

	# No types are required in Python. The philosophy says that when you
	# write or use code, it should be readable, and the type can be
	# derived from the name or the actions:
	a, b = 1, 2
	c = a + b
	print("c is an integer, because a and b are integers and c = a + b = {}"
		.format(c))

	some_amount = 1.234
	print("some_amount is a float, because amounts are generally nummeric, "
		"some_amount = {}".format(some_amount))

	some_name = "Mischa" # some_name is a String, because names are Strings
	print("some_name is a String, because names are strings, some_name = {}"
		.format(some_name))

	try:
		print("Trying to add some_name to some_amount")
		error = some_name + some_amount
	except TypeError:
		print("This raised a TypeError, because string and floats are not " +
			"compatible")


if __name__ == "__main__":
	main()