def main():
	a = 1
	b = 2
	c = 3

	x = 3.1415926
	y = 2.7182818

	print("a = {}, b = {}, c = {}".format(a, b, c))
	print("x = {}, y = {}".format(x, y))

	# Addition, subtraction, multiplication, addition:
	print("a + b + c = {}".format(a + b + c))
	print("x + y = {}".format(x + y))

	print("c - b = {}".format(c - b))
	print("y - x = {}".format(y-x))

	print("a * b * c = {}".format(a * b * c))
	print("x * c = {}".format(x * c))

	print("a / b = {}".format(a / b))
	print("x / y = {}".format(x / y))


	# Special arithmetics: Integer division, power, modulus
	print("Floor / Integer division: x // b = {}".format(x // b))
	print("Power function: x ** c = {}".format(x ** c))
	print("Modulus function: c % b = {}, x % b = {}".format(c % b, x % b))

if __name__ == "__main__":
	main()