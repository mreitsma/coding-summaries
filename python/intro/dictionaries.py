def main():

	# Dictionaries (dict) are a collection of key-value pairs. They are
	# defined using curley braces:
	d = {'someKey': 123, 'anotherKey': 'someValue'}

	print("Some dict: {}".format(d))

	# To access, use square brackets:
	print("d['someKey'] = {}".format(d['someKey']))

	# To add, use square brackets:
	d['thirdKey'] = ['some', 'list', 'of', 'words']
	print("After adding a third key, and a list: {}".format(d))

	# Dicts can be netsted:

	d['someDict'] = {'this': 'is', 'some': 'dict', 'nice': 1337}
	print("And after adding a dict in a dict: {}".format(d))

	# Some usefull build-ins, using a simple dict:
	s = {'a': 'A', 'b': 'B', 'c': 'C', 'd': 'D'}
	print("Simple dict, s = {}".format(s))

	print("List of keys: {}".format(s.keys()))
	print("List of values: {}".format(s.values()))

	# Two common exceptions that can be raised with dicts:
	# - KeyError in case of missing key:
	try:
		print("This will cause an exception: {}".format(s['e']))
	except KeyError:
		print("KeyError was raised!")
	
	# - TypeError in case the key is a non-hashable (mutable) type:
	try:
		someList = [1, 2, 3]
		s[someList] = ['Some', 'List']
	except TypeError:
		print("TypeError is raised")
	
	# Tuples are immutable and allowed:
	t = (1, 2)
	s[t] = ['1', '2']
	print("With tuple: {}".format(s))

if __name__ == "__main__":
	main()
	