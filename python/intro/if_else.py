def main():

	# There are two boolean keywords in Python: True and False
	isTrue = True
	isFalse = False

	if isTrue:
		print("This prints if isTrue is True")
	else:
		print("This prints if isTrue is False")
	
	if isFalse:
		print("This prints if isFalse is True")
	else:
		print("This prints if isFalse is False")

	x = 123
	print("x = {}".format(x))

	# (In)equalities return booleans, and can be used in if/else statements:
	
	if x == 123:
		print("x is indeed 123")
	
	if x != 312:
		print("x does not equal 321")

	if x > 100:
		print("x is bigger than 100")
	
	if x < 100:
		print("This will not print, as x is bigger than 100")
	
	# Also more complex structures:

	if x < 0:
		print("x is negative, that is not good!")
	elif x < 55:
		print("x is positive, and smaller than 55, that is still not good")
	elif x < 100:
		print("x is positive, and bigger than 55 but smaller than 100, " +
				"that is okay")
	else:
		print("x is bigger or equal to 100, that is awesome!")

	# Can also test more booleans with 'and' and 'or':

	if (x > 100) and isTrue:
		print("x is bigger than 100, and isTrue is true!")
	
	if (x < 100) or isTrue:
		print("x is smaller than 100, or isTrue is true!")
	
	# The boolean tests are short circuit, will stop evaluating as soon as
	# the answer is clear, x < 100, so it does not need to evaluate the 
	# result of is_true(), and therefore, will not call it:

	if (x < 100) and is_true():
		print("This will not print, neither will the one in is_true()")

	

def is_true():
	print("This is called from is_true()")
	return True

if __name__ == "__main__":
	main()