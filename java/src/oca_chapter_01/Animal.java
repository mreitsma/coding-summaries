package oca_chapter_01; // First line is always the package name.

import java.nio.file.Path; // Imports are after the package definition

// This is a single line comment, it only covers one line

/* This is a block comment
It spans multiple Lines
*/

/* Keywords are words with a special meaning in Java. This file has the
following keywords:
- package: Defining the name of the package.
- import: Defining a class that will be used in this class
- public: An access modifier
- private: Another access modifier
- class: Definiting a class
- this: Reference to itself. this.name means the name of the object.
*/

/* Next line defines a class, which is public (Accessible for everyone).

Every java file can have at most one public class. The name of the public class
should match the name of the file. Convention is that classes are capitalized.

Private and package private (Default access) classes can be numerous.

A class has members, which consists of fields, methods and internal classes.
*/
public class Animal {
	private String name; // This is a field

	/* The following methods has an public modifier, and a String as return
	type.
	*/
	public String getName() { // This is a method
		return name;
	}

	/* The void return type is a special type that returns nothing.

	This method has one parameter of the type String, name. The full
	method signature is public void setName(String name).

	When calling the method, an argument is supplied for the parameters. Although
	the terms parameter and argument are sometimes used interchangeably.
	*/
	public void setName(String name) {
		this.name = name;
	}

	public void sayName() {
		System.out.println("My name is " + getName());
	}

	/* When executing a compiled Java class, the JVM tries to execute the
	main() method of the class
	*/
	public static void main(String[] args) {
		Animal cat = new Animal();
		cat.setName("Nyanko sensei");
		cat.sayName();
	}

}

// A private class
class SomeOtherPrivateAnimal {
	public String name;

	Path somePathToAnAnimal;
}
