
typedef struct node {
	int value;
	struct node *next;
} node_t;

node_t* init_list(int firstValue);
void print_list_simple(node_t *head);
void add_to_end(node_t *head, int value);
node_t* add_to_beginning(node_t *head, int value);
void delete_list(node_t *head);
node_t* reverse(node_t *head);