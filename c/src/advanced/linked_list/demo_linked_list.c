#include <stdio.h>
#include "linked_list.h"

int main() {
	node_t *list = init_list(1);
	add_to_end(list, 2);
	add_to_end(list, 3);
	print_list_simple(list);
	list = add_to_beginning(list, 42);
	print_list_simple(list);

	list = reverse(list);
	print_list_simple(list);

	printf("Size of the list: %d", get_size(list));

	int stat = 0;

	int val = get_value(&stat, list, 3);
	if (stat == 0) {
		printf("Value of 3: %d", val);
	}
	else {
		printf("Error found, value of 3 not found");
	}

	val = get_value(&stat, list, 20);
	if (stat == 0) {
		printf("Value at position 20: %d", val);
	}
	else {
		printf("Error caught, no value at position 20 found");
	}

	
}