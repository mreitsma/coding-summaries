#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

node_t *init_list(int firstValue) {
	node_t *head = malloc(sizeof(node_t));
	head->value = firstValue;
	head->next=NULL;
	return head;
}

void print_list_simple(node_t *head) {
	node_t *current = head;

	printf("\n");

	while (current != NULL) {
		printf("%d\n", current->value);
		current = current->next;
	}
}

void add_to_end(node_t *head, int value) {
	node_t *current = head;
	while (current->next != NULL) {
		current = current->next;
	}
	current->next = malloc(sizeof(node_t));
	current->next->value = value;
	current->next->next = NULL;
}

node_t *add_to_beginning(node_t *head, int value) {
	node_t *newHead = malloc(sizeof(node_t));
	newHead->value = value;
	newHead->next = head;
	return newHead;
}

void delete_list(node_t *head) {
	node_t *current = head;

	while (current != NULL) {
		free(current);
		current = current->next;
	}
}

node_t *reverse(node_t *head) {

	node_t *newHead = malloc(sizeof(node_t));

	// First needs to be set, as next need to be set to NULL
	newHead->next = NULL;
	newHead->value = head->value;

	node_t *current = head->next;

	while (current != NULL) {
		newHead = add_to_beginning(newHead, current->value);
		current = current->next;
	}

	delete_list(head);
	return newHead;
}

node_t *fast_reverse(node_t *head) {

	

	return head;
}

int get_size(node_t *head) {
	int i = 0;

	node_t *current = head;
	while (current != NULL) {
		i += 1;
		current = current->next;
	}

	return i;
}

	// Status 0 is success, 1 is failed.
int get_value(int *status, node_t *head, int n) {
	status = 0;
	int i = 0;

	node_t *current = head;

	while ((i != n) && (current != NULL)) {
		i+=1;
		current = current->next;
	}

	// Reached null, without i == n, not found
	if (i != n ) {
		status = 1;
		return NULL;
	}
	return current->value;
}