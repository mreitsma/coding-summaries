#include "point.h"
#include <math.h>
#include <stdio.h>

point start_point() {
	point p;
	p.x = 0;
	p.y = 0;
	return p; 
}
point get_point(int x, int y) {
	point p;
	p.x = x;
	p.y = y;
	return p;
}

void move_x(point* p, int dx) {
	p->x += dx;
}
void move_y(point* p, int dy) {
	p->y += dy;
}
void move(point* p, int dx, int dy) {
	move_x(p, dx);
	move_y(p, dy);
}

double distance_between(point p1, point p2) {
	return sqrt( pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

void print_point(point p) {
	printf("{ x = %d, y = %d }\n", p.x, p.y);
}
