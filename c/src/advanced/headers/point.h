typedef struct {
	int x;
	int y;
} point;

point start_point();
point get_point(int x, int y);

void move_x(point *p, int dx);
void move_y(point *p, int dy);
void move(point *p, int dx, int dy);

double distance_between(point p1, point p2);

void print_point(point p);