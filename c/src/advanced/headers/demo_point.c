#include <stdio.h>
#include "point.h"

int main() {

	/* To compile and run the full thing:
	gcc -o demo_point point.c demo_point.c
	./demo_point
	*/

	printf("Creating a new point\n");
	point p = start_point();
	print_point(p);

	printf("Moving 1 unit in the x direction, and 2 in the y direction\n");
	move(&p, 1, 2);

	printf("Location after moving: ");
	print_point(p);

	printf("Creating new point with initial values\n");
	point q = get_point(-5, 4);

	printf("Second point: ");
	print_point(q);

	printf("Distance first and second point: %f\n", distance_between(p, q));

}