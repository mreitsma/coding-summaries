#include <stdio.h>

/* Pointers:
A Pointer is a variable that 'points' to a memory address. It stores an
memory address in the variable, and the value stored in that memory address
can be retrieved using the pointer.

Few fast notes on notation:

int *p -> Stores a pointer to an integer
*p = 123 -> Sets the value of the integer pointed to by *p to 123
int *p = &i -> p now is a pointer to the integer i, the & operator gives the
memory address of an already existing integer.
*/

void simple_pointer();
void array_as_pointer();
void string_example();
void pointer_arithmetic();

int main() {

	simple_pointer();
	array_as_pointer();
	string_example();
	pointer_arithmetic();

	return 0;
}

void pointer_arithmetic() {
	printf("\n\nPointer Arithmetic\n");
	
	/* Arithmetic operations ++, --, + and - work just as normal arithmetics,
	with the only difference that an increment of 1 will increment the pointer
	to point to the next memory address. This actual size of this increment
	will depent on the pointer. Increment and decrement will all work the same
	way.
	*/


	/* Character will only take one byte to store. Incrementing the memory
	address will only add a value of 1
	*/
	char c[] = {'a', 'b', 'c'};

	char *cp = &c;

	printf("The size of a char in bytes: sizeof(char) = %u\n", sizeof(char));
	printf("Initial value character pointer: cp = %u\n", cp);
	cp++;
	printf("Value after cp++: cp = %u\n", cp);

	short s[] = {123, 456, 789};

	short *sp = &s;

	printf("The size of a short in bytes: sizeof(short) = %u\n", sizeof(short));
	printf("Initial value short pointer sp = %u\n", sp);
	sp++;
	printf("Value after sp++: sp = %u\n", sp);

	int i[] = {12345, 67890, 9876, 54321};
	int *ip = &i;

	printf("The size of an int in bytes: sizeof(int) = %u\n", sizeof(int));
	printf("Initial value int pointer ip = %u\n", ip);
	
	/* Incrementing with 2 will shift the memory address by 2*4 = 8
	*/
	ip += 2;
	printf("Value after ip += 2: %u\n", ip);

}

void simple_pointer() {
	/* Create a simple integer with a specific value. Then
	create a pointer to that variable, and see how to get the
	memory address, and the value that is stored in that memory.
	*/

	printf("\n\nA Simple Pointer Example\n");

	int i = 1337;
	printf("The value in i: %d\n", i);
	printf("The value of &i, the memory address: %u\n", &i);

	int *p = &i;

	printf("Value of the pointer: %u\n", p);
	printf("Value of *p, the dereferences pointer: %d\n", *p);

	/* Both p and i now use the same memory address. Any change for
	i will reflect in p and vise versa:
	*/
	i = 7331;
	printf("New value of i: %d\n", i);
	printf("The value of *p: %d\n", *p);

	/* To change the value using a pointer, dereferencing is required.
	*/
	*p = 81447;
	printf("New value of *p: %d\n", *p);
	printf("New value of i: %d\n", i);

}

void array_as_pointer() {
	/* An array is a sequential list of values. This can be traversed using
	the square bracket notation. The list can also be traversed using a
	pointer.
	*/

	printf("\n\nArrays and Pointers\n");

	int arr[] = {123, 456, 789, 963, 842};

	printf("\nUsing the bracked notation:\n");
	for(int i = 0; i < 5; i++) {
		printf("arr[%d] = %d\n", i, arr[i]);
	}

	printf("\nUsing the pointer notation:\n");
	for(int i = 0; i < 5; i++ ) {
		printf("*(arr + %d) = %d\n", i, *(arr + i));
	}

	/* Can also create a pointer to the array, which can in turn also use the
	bracket and pointer notation.

	This line will generate a compiler warning:
	warning: incompatible pointer types initializing 'int *' with an expression
	of type 'int (*)[5]' [-Wincompatible-pointer-types]
	*/
	int *p = &arr;

	printf("\nUsing the bracked notation:\n");
	for(int i = 0; i < 5; i++) {
		printf("p[%d] = %d\n", i, p[i]);
	}

	printf("\nUsing the pointer notation:\n");
	for(int i = 0; i < 5; i++ ) {
		printf("*(p + %d) = %d\n", i, *(p + i));
	}

	/* Using the variable p (without the *) will show us the memory addresses.
	These are the same as the values in &arr[];
	*/

	printf("\nPrinting memory addresses using &arr[] and p:\n");
	for(int i = 0; i < 5; i++) {
		printf("&arr[%d] = %u, (p + %d) = %u\n", i, &arr[i], i, p + i);
	}

	/* To understand what is happening, take the following example for the
	output of *(p + i) and p + i:

	Using the pointer notation:
	*(p + 0) = 123
	*(p + 1) = 456
	*(p + 2) = 789
	*(p + 3) = 963
	*(p + 4) = 842

	Printing memory addresses using &arr[] and p:
	&arr[0] = 3787348592, (p + 0) = 3787348592
	&arr[1] = 3787348596, (p + 1) = 3787348596
	&arr[2] = 3787348600, (p + 2) = 3787348600
	&arr[3] = 3787348604, (p + 3) = 3787348604
	&arr[4] = 3787348608, (p + 4) = 3787348608


	Notice that the addresses differences are a multiple of 4, becuase
	integers on the machine where this code ran use 4 bytes to represent an
	integer:

	Memory address	Binary Value	Int value
	3787348592		0000 0000
	3787348593		0000 0000
	3787348594		0000 0000
	3787348595		0111 1011		123

	3787348596		0000 0000
	3787348597		0000 0000
	3787348598		0000 0001
	3787348599		1100 1000		456

	3787348600		0000 0000
	3787348601		0000 0000
	3787348602 		0000 0011
	3787348603  	0001 0101		789

	3787348604		0000 0000
	3787348605		0000 0000
	3787348606		0000 0011
	3787348607		1100 0011		963

	3787348608		0000 0000
	3787348609		0000 0000
	3787348610		0000 0011
	3787348611		0100 1010		842
	*/

}

void string_example() {

	printf("\n\nPointers as Strings\n");

	/* A string is an array of characters.
	*/
	char * myName = "Mischa Reitsma";

	/* The %s takes a char pointer:
	*/
	printf("My name is %s\n", myName);

	/* It is not seen in the initalization, but a character sequence
	always ends with the null character. This is used by C to determine
	where the end of a character sequence is found.
	*/
	int i;
	for (i = 0; i < 14; i++) {
		printf("*(myName + %d) = %c\n", i, *(myName + i));
	}

	/* Next character is the null characters. This will not print (maybe as a
	box or question mark on some systems). The int value of this is zero.
	*/
	printf("*(myName + %d) = int(%c) = %d\n", i, *(myName + i), *(myName + i));
}