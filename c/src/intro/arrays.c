#include <stdio.h>

void print_1d_array(int size, int arr[size]);
void print_2d_array(int rows, int columns, int arr[rows][columns]);
void print_2d_array_big(int rows, int columns, int arr[rows][columns]);

int main() {

	/* For 1D arrays, there are two ways to create them:
	1. Using <type> <varname>[] = { <some>, <init>, <list> };
	2. Using <type> <varname>[size]; and setting individual elements.
	*/
	int arr1[] = {1, 2, 3};
	printf("arr1: ");
	print_1d_array(3, arr1);

	int arr2[5];
	arr2[0] = 9;
	arr2[1] = 8;
	arr2[2] = 7;
	arr2[3] = 6;
	arr2[4] = 5;
	printf("\n\narr2: ");
	print_1d_array(5, arr2);

	int arr2d1[2][3];

	arr2d1[0][0] = 1; arr2d1[0][1] = 2; arr2d1[0][2]=3;
	arr2d1[1][0] = 4; arr2d1[1][1] = 5; arr2d1[1][2]=6;
	printf("\n\narr2d1 as oneliner and full: \n");
	print_2d_array(2, 3, arr2d1);
	print_2d_array_big(2, 3, arr2d1);

	/* Using initalization, 2D arrays can be defined in three ways:
	- Using tiered curly braces
	- Using single curly braces
	- Allow to ommit the x-dimension
	*/

	int arr2d2[3][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
	printf("\n\nUsing multiple curly braces:\n");
	print_2d_array_big(3, 3, arr2d2);

	int arr2d3[3][3] = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	printf("\n\nUsing single curly braces:\n");
	print_2d_array_big(3, 3, arr2d3);

	/* The compiler is smart, and knows the first dimension because of the
	second dimension, and the total number of elements (10 / 5 = 2);
	*/
	int arr2d4[][5] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
	printf("\n\nOmmiting the first dimension:\n");
	print_2d_array_big(2, 5, arr2d4);

	return 0;
}

void print_1d_array(int size, int arr[size]) {
	printf("{ ");
	for(int i = 0; i < size - 1; i++) {
		printf("%d, ", arr[i]);
	}
	printf("%d }\n", arr[size-1]);
}

void print_2d_array(int rows, int columns, int arr[rows][columns]) {
	printf("{");
	for(int i = 0; i < rows; i++) {
		printf("{ ");
		for(int j = 0; j < columns - 1; j++) {
			printf("%d, ", arr[i][j]);
		}
		printf("%d }", arr[i][columns-1]);
		if ( i != rows - 1) {
			printf(", ");
		}
	}
	printf("}\n");
}

void print_2d_array_big(int rows, int columns, int arr[rows][columns]) {
	for(int i = 0; i < rows; i++) {
		printf( "| ");
		for(int j = 0; j < columns; j++) {
			printf("%d ", arr[i][j]);
		}
		printf("|\n");
	}
}