#include <stdio.h>
#include <limits.h>

/* c_types: Examples for all the primite types in C.
*/



void demo_int();
void demo_uint();
void demo_float();
void demo_bool();

int main() {
	demo_int();
	demo_uint();
	demo_float();
	demo_bool();
	return 0;
}


/* Demo integers.

C knows a few types of integers:
A char, int, long and long long.
*/
void demo_int() {
	printf("\n\nInside demo_int()\n");
	printf("-----------------\n");

	printf("Size of the various integer types in bytes:\n");
	printf("sizeof(char): %lu\n", sizeof(char));
	printf("sizeof(short): %lu\n", sizeof(short));
	printf("sizeof(int): %lu\n", sizeof(int));
	printf("sizeof(long): %lu\n", sizeof(long));
	printf("sizeof(long long): %lu\n", sizeof(long long));
	printf("The results depend on the machine\n\n");

	/* Alphanumeric values can be assigned to chars, and their byte value will
	be used as numeric value:
	*/
	char c = 'A';
	printf("char c: %c\n", c);
	printf("c + 20: %c\n", c + 20);
	printf("'B' - 'A', using %%d to format: %d\n", 'B' - 'A');
	
	/* For int, long and long long, assign integer values. A numeric literal
	in the code is an int by default. To use longs and long longs, use the L
	and LL suffixed:

	long l = 123L
	long long ll = 123456LL
	*/

	int i = 123;
	long l = INT_MAX + 100L;	// Max integer value + some number
	long long ll = l * 100LL;

	/* For formatting, use %d, %ld and %lld:
	*/
	printf("Use %%d for digits, i = %d\n", i);
	printf("Use %%ld for long digits, l = %ld\n", l);
	printf("Use %%lld for long long digits, ll = %lld\n", ll);
}

/* Unsinged ints are the same as signed ints, however, the bit that is used for
the sign in signed ints can also be used for the value. Therefore, the max
value is twice as big as the unsigned versions.

The unsigned types avaliable:
unsigned char, unsigned int, unsigned long, unsigned long long

*/
void demo_uint() {
	printf("\n\nInside demo_uint()\n");
	printf("------------------\n");

	/*For unsigned variables, us the following suffixes:
	
	- U for ints
	- UL for longs
	- ULL for long longs
	*/

	unsigned char c = 'a';
	unsigned int i = INT_MAX + 10U; // no overflow, can go higher than signed
	unsigned short s = SHRT_MAX + 10U; // No overflow, can go higher than signed
	unsigned long l = LONG_MAX + 10UL; // no overflow, can go higher than signed
	unsigned long long ll = LONG_MAX +20ULL; // no overflow error, can go higher than signed

	printf("Use %%hhu for unsigned chars: c = %hhu\n", c);
	printf("Use %%hu for unsigned shorts: s = %hu\n", s);
	printf("Use %%u for unsigned ints: i = %u\n", i);
	printf("Use %%lu for unsigned long: l = %lu\n", l);
	printf("Use %%llu for unsigned long long: ll = %llu\n", ll);

	/* Do take care of proper formatting when printing the ints. When
	printing an unsigned int with %d or a signed int with %u, results
	are invalid
	*/

	unsigned int u = INT_MAX + 1U;
	int v = -1;

	printf("Printing the unsigned int, with value INT_MAX + 1 with %%d: %d\n", u);
	printf("Printing the signed int, with value -1 with %%u: %u\n", v);

}

void demo_float() {

	printf("\n\nInside demo_float()\n");
	printf("-------------------\n");

	float f = 3.1415926;
	double d = 123.123;

	printf("Formatting options include rounding %.2f\n", f);
	printf("For both doubles and floats, use %%f, %f\n", d);
}

/* Demo boolean

In C, there is no boolean. Instead of booleans, the values 0 an 1 are often
used as shown in this example.
*/
void demo_bool() {

	printf("\n\nInside demo_bool()\n");
	printf("------------------\n");

	/* Normally, booleans are defined on the top of the file like this:
	*/
	#define BOOL char
	#define FALSE 0
	#define TRUE 1

	// Now, BOOLs can used:
	BOOL isTrue  = TRUE;
	BOOL isFalse = FALSE;

	printf("isTrue: %d, isFalse: %d\n", isTrue, isFalse);

	BOOL isDone = FALSE;

	int i = 0;

	/* In boolean expressions, 0 is treated as false, any other digit is
	treated as true. It can be used in boolean expressions without comparing
	the value to 0 or 1
	*/
	while( ! isDone ) {
		printf("In while loop, i = %d\n", i);
		i+=1;
		if (i > 5) {
			isDone = TRUE;
		}
	}
}
