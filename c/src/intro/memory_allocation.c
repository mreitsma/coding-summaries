#include <stdio.h>
#include <stdlib.h>

/* A struct with two simple, fixed size members.
*/
typedef struct {
	int x;
	int y;
} point_t;

/* A struct with a pointer to a variable sized string
*/
typedef struct {
	int age;
	char* name;
} person_t;

void simple_allocation();
void non_square_2d_arrays();
void print_point();

int main() {
	simple_allocation();
	non_square_2d_arrays();
}

void simple_allocation() {
	printf("\n\nInsode simple_allocation()\n");
	printf("--------------------------\n");

	/* To allocate memory for a variable, the malloc() functions is used.
	This malloc() function will reserve memory required for the
	variable and returns a pointer to that memory
	*/
	point_t *p = malloc(sizeof(point_t));

	/* In order to set the values, a dereference is required:
	*/
	(*p).x = 123;

	/* A short hand notation for dereferencing + accessing members can
	be achieved by using ->:
	*/
	p->y = 456;

	print_point(*p);

	free(p);

}

void non_square_2d_arrays() {
	printf("\n\nInside non_square_2d_arrays()\n");
	printf("-----------------------------\n");

	/* If we take the pascal triangle as example, a non-square array
	can be usefull. This means that the first pointer allocates a the
	memory required for a pointer. The second allocation can allocate a
	specific size of ints.
	*/

	int **pp =  (int **) malloc(5 * sizeof(int *));

	/* First row: 1 */

	pp[0] = malloc(sizeof(int));
	pp[0][0] = 1;

	pp[1] = malloc(2 * sizeof(int));
	pp[1][0] = 1; pp[1][1] = 1;

	pp[2] = malloc(3 * sizeof(int));
	pp[2][0] = 1; pp[2][1] = 2; pp[2][2] = 1;

	pp[3] = malloc(4 * sizeof(int));
	pp[3][0] = 1; pp[3][1] = 3; pp[3][2] = 3; pp[3][3] = 1;

	pp[4] = malloc(5 * sizeof(int));
	pp[4][0] = 1; pp[4][1] = 4; pp[4][2] = 6; pp[4][3] = 4; pp[4][4] = 1;

	/* Only allocate that memory that is required, do not need to allocate
	25 ints, but only 1 + 2 + 3 + 4 + 5 = 15 ints.
	*/

	for (int i = 0; i < 5; i++ ){
		for(int j = 0; j <= i; j++ ) {
			printf("%d ", pp[i][j]);
		}
		printf("\n");
	}
	free(pp[0]); free(pp[1]); free(pp[2]); free(pp[3]); free(pp[4]);
	free(pp);

}

void print_point(point_t p) {
	printf("{ %d, %d }", p.x, p.y);
}