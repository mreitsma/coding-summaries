#include <stdio.h>

void do_nothing(int i);
int get_incremented(int i);
void increment(int *i);

int main() {
	/* C always passes a copy of the variable with the same value. This
	can be seen by passing an integer to the method do_nothing().
	*/

	int i = 7;
	printf("Value of i before do_nothing(): %d\n", i);
	do_nothing(i);
	printf("Value of i after do_nothing(): %d\n\n", i);

	/* A way of doing something with a value, is returing the result:
	*/

	printf("Value of i before get_incremented(): %d\n", i);
	i = get_incremented(i);
	printf("Value of i after get_incremented(): %d\n\n", i);

	/* However, same can be achieved by using pointers. The pointer variable
	will be a copy of the original, however, still has the same value (points
	to the same memory address), and therefore, exposes the variable.
	*/

	printf("Value of i before increment(): %d\n", i);
	increment(&i);
	printf("Value of i after increment(): %d\n\n", i);

	return 0;
}

void do_nothing(int i) {
	// All useless operations, result will not reflect in calling value.
	i = i + 123;
	i = i / 100;
}

int get_incremented(int i) {
	return i + 1;
}

void increment(int *i) {
	*i = *i + 1;
}
