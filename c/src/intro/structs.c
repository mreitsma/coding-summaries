#include <stdio.h>

/* Structs are data structures that can be defined as following:
*/
struct point2d {
	int x;
	int y;
};

/* The typedef keyword makes it easier to use, as the struct name can be
used as type:
*/
typedef struct {
	int x;
	int y;
	int z;
} point3d;

void print_point2d(struct point2d p);
void print_point3d(point3d p);

int main() {
	// Create struct without typedef:
	struct point2d p2d;
	p2d.x = 123;
	p2d.y = 456;
	print_point2d(p2d);

	// Create struct with typedef:
	point3d p3d;
	p3d.x = 1;
	p3d.y = 33;
	p3d.z = 7;
	print_point3d(p3d);

	return 0;
}

void print_point2d(struct point2d p) {
	printf("{ x = '%d', y = '%d'}\n", p.x, p.y);
}

void print_point3d(point3d p) {
		printf("{ x = '%d', y = '%d', z = '%d'}\n", p.x, p.y, p.z);
}